<?php
/**
 * ${NAME}
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/3
 */

define('H_TOOL_VERSION', '1.0');

// 环境类
defined('C_ENV_LOCAL') or define('C_ENV_LOCAL', 'local');
defined('C_ENV_DEV') or define('C_ENV_DEV', 'dev');
defined('C_ENV_TEST') or define('C_ENV_TEST', 'test');
defined('C_ENV_PRE') or define('C_ENV_PRE', 'pre');
defined('C_ENV_PROD') or define('C_ENV_PROD', 'prod');
defined('C_ENV_SANDBOX') or define('C_ENV_SANDBOX', 'sandbox');

// bool类
defined('C_TRUE') or define('C_TRUE', true);
defined('C_FALSE') or define('C_FALSE', false);
defined('C_INT_TRUE') or define('C_INT_TRUE', 1);
defined('C_INT_FALSE') or define('C_INT_FALSE', 0);
defined('C_STR_YES') or define('C_STR_YES', 'yes');
defined('C_STR_NO') or define('C_STR_NO', 'no');
defined('C_STR_ON') or define('C_STR_ON', 'on');
defined('C_STR_OFF') or define('C_STR_OFF', 'off');
defined('C_STR_ENABLE') or define('C_STR_ENABLE', 'enable');
defined('C_STR_DISABLE') or define('C_STR_DISABLE', 'disable');
defined('C_STR_OPEN') or define('C_STR_OPEN', 'open');
defined('C_STR_CLOSE') or define('C_STR_CLOSE', 'close');

// 空值类
defined('C_EMPTY_STRING') or define('C_EMPTY_STRING', '');
defined('C_EMPTY_ARRAY') or define('C_EMPTY_ARRAY', []);
defined('C_EMPTY_NULL') or define('C_EMPTY_NULL', null);
defined('C_EMPTY_STR_NULL') or define('C_EMPTY_STR_NULL', 'null');
defined('C_EMPTY_OBJECT') or define('C_EMPTY_OBJECT', (object)[]);
defined('C_EMPTY_UNDEFINED') or define('C_EMPTY_UNDEFINED', 'undefined');
defined('C_EMPTY') or define('C_EMPTY', [
    C_EMPTY_STRING,
    C_EMPTY_ARRAY,
    C_EMPTY_NULL,
    C_EMPTY_STR_NULL,
    C_EMPTY_OBJECT,
    C_EMPTY_UNDEFINED,
]);

// 数值类
defined('C_PI') or define('C_PI', 3.14);
defined('C_PI_9') or define('C_PI_9', 3.141592653);
defined('C_CYCLE_NUMBER') or define('C_CYCLE_NUMBER', 142857);
defined('C_BYTE') or define('C_BYTE', 1);
defined('C_KILO_BYTE') or define('C_KILO_BYTE', 1024);
defined('C_MEGA_BYTE') or define('C_MEGA_BYTE', 1024 * 1024);
defined('C_GIGA_BYTE') or define('C_GIGA_BYTE', 1024 * 1024 * 1024);
defined('C_TERA_BYTE') or define('C_TERA_BYTE', 1024 * 1024 * 1024 * 1024);
defined('C_PETA_BYTE') or define('C_PETA_BYTE', 1024 * 1024 * 1024 * 1024 * 1024);

// 时间类
defined('C_TIME_FORMAT_YMD_START') or define('C_TIME_FORMAT_YMD_START', 'Y-m-d 00:00:00');
defined('C_TIME_FORMAT_YMD_END') or define('C_TIME_FORMAT_YMD_END', 'Y-m-d 23:59:59');
defined('C_TIME_FORMAT_YMD_HIS') or define('C_TIME_FORMAT_YMD_HIS', 'Y-m-d H:i:s');
defined('C_TIME_FORMAT_YMD_HIS_CN') or define('C_TIME_FORMAT_YMD_HIS_FULL_CN', 'Y年m月d日 H时i分s秒');
defined('C_TIME_FORMAT_YMD_HIS_FULL') or define('C_TIME_FORMAT_YMD_HIS_FULL', 'Y-m-d H:i:s.u');

// 符号类
defined('C_UNDERSCORE') or define('C_UNDERSCORE', '_');
defined('C_HORIZONTAL_LINE') or define('C_HORIZONTAL_LINE', '-');
defined('C_VERTICAL_LINE') or define('C_VERTICAL_LINE', '|');
defined('C_COLON') or define('C_COLON', ':');
defined('C_COMMA') or define('C_COMMA', ',');
defined('C_SEMICOLON') or define('C_SEMICOLON', ';');
defined('C_PERIOD') or define('C_PERIOD', '.');
defined('C_QUESTION_MARK') or define('C_QUESTION_MARK', '?');
defined('C_PUNCTUATION') or define('C_PUNCTUATION', '#');
