<?php

namespace h\tool\utils\slice;

/**
 * 抽奖
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */
class PrizeUtils extends StaticUtils
{
    const F_PRIZE_NO = 'prize_no';
    const F_PRIZE_NAME = 'prize_name';
    const F_PRIZE_QUANTITIES = 'prize_quantities';
    const F_PRIZE_PROBABILITY = 'prize_probability';
    const F_PRIZE_PIC_PATH = 'prize_pic_path';

    /**
     * 数量无穷大
     */
    const QUANTITIES_INF = INF;

    /**
     * @var int 中奖概率总和,默认： 100
     */
    protected int $sumProbability = 100;

    /**
     * @var array 奖品列表
     */
    private array $_prizes = [

    ];

    /**
     * @var array 中奖信息
     */
    private array $_winPrize = [];

    /**
     * @var int|float 中奖数字
     */
    private $_winNumber = 0;

    /**
     * 增加奖品
     * @param string $no 编号
     * @param string $name 名称
     * @param int|float $probability 概率
     * @param int|float $quantities 数量
     * @param string $picPath 图片路径
     * @return $this
     */
    final public function addPrize(string $no, string $name, $probability = 0, $quantities = self::QUANTITIES_INF, string $picPath = '')
    {
        if (isset($this->_prizes[$no])) {
            return $this;
        }

        $this->_prizes[$no] = [
            self::F_PRIZE_NO => $no,
            self::F_PRIZE_NAME => $name,
            self::F_PRIZE_QUANTITIES => $quantities,
            self::F_PRIZE_PROBABILITY => $probability,
            self::F_PRIZE_PIC_PATH => $picPath,
        ];
        return $this;
    }

    /**
     * 移除奖品
     * @param $no
     * @return $this
     */
    final public function removePrize($no)
    {
        unset($this->_prizes[$no]);
        return $this;
    }

    /**
     * 获取奖品列表
     * @return array
     */
    final public function getPrizes()
    {
        return $this->_prizes;
    }

    /**
     * 获取中奖奖品
     * @return array
     */
    final public function getWinPrize()
    {
        return $this->_winPrize;
    }

    /**
     * 设置中奖数字
     * @return float|int
     */
    final public function getWinNumber()
    {
        return $this->_winNumber;
    }

    /**
     * 设置中奖信息
     * @param array $prize
     * @return $this
     */
    protected function setWinPrize(array $prize = [])
    {
        $this->_winPrize = $prize;
        return $this;
    }

    /**
     * 抽奖
     * @return $this
     */
    final public function drawPrize()
    {
        if (empty($this->getPrizes())) {
            throw new \Exception('未设置奖品信息');
        }

        $this->sortPrizes();

        $randomNumber = $this->randNumberWithProbability();

        $this->winPrizeAlgorithm($randomNumber);

        return $this;
    }

    /**
     * 抽奖算法
     * @inheritDoc 可重写
     * @param int|float $randomNumber
     * @return void
     */
    protected function winPrizeAlgorithm($randomNumber)
    {
        // 根据随机数确定中奖奖品
        $probabilitySum = 0;
        foreach ($this->getPrizes() as $no => $prize) {
            $probability = $prize[self::F_PRIZE_PROBABILITY];
            $quantities = $prize[self::F_PRIZE_QUANTITIES];
            if ($quantities <= 0) {
                break;
            }
            $probabilitySum += $probability;
            if ($randomNumber <= $probabilitySum) {
                $this->setWinPrize($prize);
                break;
            }
        }
    }

    /**
     * 根据概率生成随机数
     * @return int|float
     */
    private function randNumberWithProbability()
    {
        // 计算总概率
        $totalProbability = array_sum(array_column($this->getPrizes(), self::F_PRIZE_PROBABILITY));
        $totalProbability = number_format($totalProbability, 2);

        if (!($totalProbability <=> $this->sumProbability)) {
            throw new \Exception(sprintf('奖品概率总和(%s)不等于%s', $totalProbability, $this->sumProbability));
        }

        // 生成随机数
        $exp = 9;
        $randomNumber = round(mt_rand(1, pow($this->sumProbability, $exp)) / pow($this->sumProbability, $exp - 1), 4);
        
        $this->_winNumber = $randomNumber;

        return $this->_winNumber;
    }

    /**
     * 排序奖品
     * @return void
     */
    private function sortPrizes()
    {
        $sortFields = array_column($this->getPrizes(), self::F_PRIZE_PROBABILITY);
        array_multisort($sortFields, SORT_DESC, $this->_prizes);
    }
}