<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\IInstance;

/**
 * 静态模式
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
abstract class StaticUtils implements IInstance
{
    /**
     * 获取实例
     * @return static
     */
    final public static function getInstance(): object
    {
        $class = get_called_class();
        return new $class;
    }
}