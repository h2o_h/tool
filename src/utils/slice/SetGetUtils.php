<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\ISetterGetter;
use h\tool\utils\helper\NomenclatureHelper;

/**
 * SetGetUtils
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
class SetGetUtils extends StaticUtils implements ISetterGetter
{
    public function __get($name)
    {
        return $this->getter($name);
    }

    public function __set($name, $value)
    {
        $this->setter($name, $value);
        return $this;
    }

    private function parseSetterOrGetter(string $attr, string $prefix): string
    {
        return $prefix . NomenclatureHelper::underscore2PascalCase($attr);
    }

    /**
     * 设置属性值
     * @param string $attr
     * @param $value
     * @return ISetterGetter
     */
    public function setter(string $attr, $value): ISetterGetter
    {
        $setter = $this->parseSetterOrGetter($attr, self::PREFIX_SETTER);
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            $this->$attr = $value;
        }

        return $this;
    }

    /**
     * 获取属性值
     * @param string $attr
     * @param $default
     * @return mixed|null
     */
    public function getter(string $attr, $default = null)
    {
        $getter = $this->parseSetterOrGetter($attr, self::PREFIX_GETTER);

        if (method_exists($this, $getter)) {
            $value = $this->$getter($attr);
        } else {
            $value = $this->$attr ?? $default;
        }

        return $value;
    }
}