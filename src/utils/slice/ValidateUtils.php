<?php

namespace h\tool\utils\slice;

use h\tool\utils\slice\validate\IValidate;

/**
 * 验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class ValidateUtils extends StaticUtils
{
    /**
     * @var mixed 待验证值
     */
    private $_value;

    /**
     * @var bool 验证结果
     */
    private bool $_result = true;

    /**
     * @var array 错误信息
     */
    private array $errors = [];

    /**
     * @var array<IValidate>|IValidate[] 验证类
     */
    private array $_validates = [];

    /**
     * 获取待验证值
     * @return mixed
     */
    final public function getValue()
    {
        return $this->_value;
    }

    /**
     * 设置待验证值
     * @param mixed $value
     */
    final public function setValue($value): void
    {
        $this->_value = $value;
    }

    /**
     * 获取验证结果
     * @return bool
     */
    final public function isResult(): bool
    {
        return $this->_result;
    }

    /**
     * 设置结果
     * @param bool $result
     * @return void
     */
    final public function setResult(bool $result): void
    {
        $this->_result = $result;
    }

    /**
     * 获取错误信息
     * @return array
     */
    final public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * 增加错误信息
     * @param string $errors
     * @return $this
     */
    final public function addErrors(string $errors)
    {
        $this->errors[] = $errors;

        return $this;
    }

    /**
     * 增加验证器
     * @param IValidate $validate
     * @return $this
     */
    final public function addValidate(IValidate $validate)
    {
        $class = get_class($validate);
        $this->_validates[$class] = $validate;

        return $this;
    }

    /**
     * 验证
     * @return $this
     */
    final public function validate()
    {
        foreach ($this->_validates as $validate) {
            $validate->setValidateSlice($this);
            $validate->verify();
        }

        return $this;
    }
}