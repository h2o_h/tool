<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\to\IToArray;

/**
 * 具有动态属性的通用空类
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
class StdUtils extends \stdClass implements IToArray
{
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}