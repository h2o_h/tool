<?php

namespace h\tool\utils\slice\validate;

/**
 * 身份证号验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class IdCardValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^\d{17}[\d|x]|\d{15}$/';
    }

    protected function patternErrorTip(): string
    {
        return '身份证号格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}