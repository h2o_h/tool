<?php

namespace h\tool\utils\slice\validate;

/**
 * QQ验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class QqValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^[1-9]([0-9]{5,11})$/';
    }

    protected function patternErrorTip(): string
    {
        return 'QQ号格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}