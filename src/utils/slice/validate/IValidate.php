<?php

namespace h\tool\utils\slice\validate;

use h\tool\utils\slice\ValidateUtils;

/**
 * IValidate
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
interface IValidate
{
    public function setValidateSlice(ValidateUtils $validateSlice);

    public function verify();
}