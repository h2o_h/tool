<?php

namespace h\tool\utils\slice\validate;

/**
 * 网址验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class UrlValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^[a-zA-z]+://[^\s]*$/';
    }

    protected function patternErrorTip(): string
    {
        return '网址格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}