<?php

namespace h\tool\utils\slice\validate;

/**
 * IPV4验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class IpV4Validate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)\.(25[0-5]|2[0-4]\d|[0-1]\d{2}|[1-9]?\d)$/';
    }

    protected function patternErrorTip(): string
    {
        return 'IPV4格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}