<?php

namespace h\tool\utils\slice\validate;

/**
 * Email验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class EmailValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/';
    }

    protected function patternErrorTip(): string
    {
        return '邮箱格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}