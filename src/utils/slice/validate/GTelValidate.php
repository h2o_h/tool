<?php

namespace h\tool\utils\slice\validate;

/**
 * 固话号码(国内)验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class GTelValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^[0-9-()（）]{7,18}$/';
    }

    protected function patternErrorTip(): string
    {
        return '固话号码(国内)格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}