<?php

namespace h\tool\utils\slice\validate;

/**
 * 中文字符验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class CnValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^[\u4e00-\u9fa5]$/';
    }

    protected function patternErrorTip(): string
    {
        return '只能是中文字符';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}