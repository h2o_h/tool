<?php

namespace h\tool\utils\slice\validate;

use h\tool\utils\slice\StaticUtils;
use h\tool\utils\slice\ValidateUtils;

/**
 * AbstractValidate
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
abstract class AbstractValidate extends StaticUtils implements IValidate
{
    /**
     * @var ValidateUtils 验证器
     */
    private ValidateUtils $_vs;

    /**
     * 设置验证器
     * @param ValidateUtils $validateSlice
     * @return $this
     */
    final public function setValidateSlice(ValidateUtils $validateSlice)
    {
        $this->_vs = $validateSlice;

        return $this;
    }

    /**
     * 获取验证器
     * @return ValidateUtils
     */
    final protected function getVs()
    {
        return $this->_vs;
    }

    /**
     * @return string 定义正则表达式
     */
    protected function pattern(): string
    {
        return '/.*/';
    }

    /**
     * @return string 定义正则表达式错误提示
     */
    protected function patternErrorTip(): string
    {
        return '格式错误';
    }

    /**
     * 正则匹配
     * @return $this
     */
    final protected function pregMatch()
    {
        $pattern = $this->pattern();
        if (!preg_match($pattern, $this->getVs()->getValue(), $matches)) {
            $this->getVs()->addErrors($this->patternErrorTip());
            $this->getVs()->setResult(false);
        } else {
            $this->getVs()->setResult(true);
        }

        return $this;
    }
}