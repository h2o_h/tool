<?php

namespace h\tool\utils\slice\validate;

/**
 * 手机号码验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class MobileValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^0?(13|14|15|17|18|19)[0-9]{9}$/';
    }

    protected function patternErrorTip(): string
    {
        return '手机号码格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}