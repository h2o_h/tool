<?php

namespace h\tool\utils\slice\validate;

/**
 * 邮政编码(国内)验证器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class GPostCodeValidate extends AbstractValidate
{
    protected function pattern(): string
    {
        return '/^\d{6}$/';
    }

    protected function patternErrorTip(): string
    {
        return '邮政编码(国内)格式错误';
    }

    public function verify()
    {
        $this->pregMatch();
    }
}