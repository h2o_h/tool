<?php

namespace h\tool\utils\slice;

/**
 * 随机密码
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class PwdUtils extends StaticUtils
{
    /**
     * @var string 数字
     */
    protected string $numbers = '0123456789';

    /**
     * @var string 特殊字符
     */
    protected string $special = '~!@#$%^&*()_+';

    /**
     * @var string 小写字母
     */
    protected string $lcLetters = 'abcdefghijklmnpqrstuvwxyz';

    /**
     * @var string 大写字母
     */
    protected string $ucLetters = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';

    /**
     * @var string 所有字符
     */
    private string $_string = '';

    /**
     * @var string 密码
     */
    private string $_pwd = '';

    /**
     * @var int 数字长度
     */
    private int $_lenNumbers = 3;

    /**
     * @var int 特殊字符长度
     */
    private int $_lenSpecial = 1;

    /**
     * @var int 小写字母长度
     */
    private int $_lenLcLetters = 2;

    /**
     * @var int 大写字母长度
     */
    private int $_lenUcLetters = 2;

    /**
     * @var int 最小长度
     */
    private int $lenMin = 6;

    /**
     * @return string 获取数字
     */
    public function getNumbers(): string
    {
        return $this->numbers;
    }

    /**
     * 设置数字
     * @param string $numbers
     * @return $this
     */
    public function setNumbers(string $numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * 获取特殊字符
     * @return string
     */
    public function getSpecial(): string
    {
        return $this->special;
    }

    /**
     * 设置特殊字符
     * @param string $special
     * @return $this
     */
    public function setSpecial(string $special)
    {
        $this->special = $special;

        return $this;
    }

    /**
     * 获取小写字母
     * @return string
     */
    public function getLcLetters(): string
    {
        return $this->lcLetters;
    }

    /**
     * 设置小写字母
     * @param string $lcLetters
     * @return $this
     */
    public function setLcLetters(string $lcLetters)
    {
        $this->lcLetters = $lcLetters;

        return $this;
    }

    /**
     * 获取大写字母
     * @return string
     */
    public function getUcLetters(): string
    {
        return $this->ucLetters;
    }

    /**
     * 设置大写字母
     * @param string $ucLetters
     * @return $this
     */
    public function setUcLetters(string $ucLetters)
    {
        $this->ucLetters = $ucLetters;

        return $this;
    }

    /**
     * 获取数字长度
     * @return int
     */
    public function getLenNumbers(): int
    {
        return $this->_lenNumbers;
    }

    /**
     * 设置数字长度
     * @param int $lenNumbers
     * @return $this
     */
    public function setLenNumbers(int $lenNumbers)
    {
        $this->_lenNumbers = $lenNumbers;

        return $this;
    }

    /**
     * 获取特殊字符长度
     * @return int
     */
    public function getLenSpecial(): int
    {
        return $this->_lenSpecial;
    }

    /**
     * 设置特殊字符长度
     * @param int $lenSpecial
     * @return $this
     */
    public function setLenSpecial(int $lenSpecial)
    {
        $this->_lenSpecial = $lenSpecial;

        return $this;
    }

    /**
     * 获取小写字母长度
     * @return int
     */
    public function getLenLcLetters(): int
    {
        return $this->_lenLcLetters;
    }

    /**
     * 设置小写字母长度
     * @param int $lenLcLetters
     * @return $this
     */
    public function setLenLcLetters(int $lenLcLetters)
    {
        $this->_lenLcLetters = $lenLcLetters;

        return $this;
    }

    /**
     * 获取大写字母长度
     * @return int
     */
    public function getLenUcLetters(): int
    {
        return $this->_lenUcLetters;
    }

    /**
     * 设置大写字母长度
     * @param int $lenUcLetters
     * @return $this
     */
    public function setLenUcLetters(int $lenUcLetters)
    {
        $this->_lenUcLetters = $lenUcLetters;

        return $this;
    }

    /**
     * 获取最小长度
     * @return int
     */
    public function getLenMin(): int
    {
        return $this->lenMin;
    }

    /**
     * 设置最小长度
     * @param int $lenMin
     * @return $this
     */
    public function setLenMin(int $lenMin)
    {
        $this->lenMin = $lenMin;

        return $this;
    }

    /**
     * 生成密码
     * @return string
     */
    final public function gen()
    {
        $this->_pwd = '';

        $this->checkLen();

        $this->assemblePwd();

        return str_shuffle($this->_pwd);
    }

    private function assemblePwd()
    {
        $this->getPwdWithLen($this->getLenNumbers(), $this->getNumbers());
        $this->getPwdWithLen($this->getLenLcLetters(), $this->getLcLetters());
        $this->getPwdWithLen($this->getLenUcLetters(), $this->getUcLetters());
        $this->getPwdWithLen($this->getLenSpecial(), $this->getSpecial());
    }

    private function getPwdWithLen(int $len, string $string)
    {
        $string = str_shuffle($string);
        for ($i = 0; $i < $len; $i++) {
            $this->_pwd .= $string[rand(0, strlen($string) - 1)];
        }
    }

    private function checkLen()
    {
        if ($this->getLenMin() < 6) {
            throw new \Exception('最小长度不能小于 6 位');
        }

        $lens = [
            $this->getLenNumbers(),
            $this->getLenLcLetters(),
            $this->getLenUcLetters(),
            $this->getLenSpecial(),
        ];

        $sumLens = array_sum($lens);

        if ($sumLens < $this->getLenMin()) {
            $this->setLenNumbers($this->getLenNumbers() + ($this->getLenMin() - $sumLens));
        }
    }
}