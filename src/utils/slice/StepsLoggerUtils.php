<?php

namespace h\tool\utils\slice;

/**
 * 步骤记录器
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class StepsLoggerUtils extends StaticUtils
{
    /**
     * @var string 请求ID
     */
    protected string $requestId = '';

    /**
     * @var string 功能名称
     */
    protected string $fnName = '';

    /**
     * @var array 步骤记录
     */
    protected array $steps = [];

    /**
     * 获取请求ID
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->requestId;
    }

    /**
     * 设置请求ID
     * @param string $requestId 请求ID
     * @return void
     */
    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

    /**
     * 获取功能名称
     * @return string
     */
    public function getFnName(): string
    {
        return $this->fnName;
    }

    /**
     * 设置功能名称
     * @param string $fnName 功能名称
     * @return void
     */
    public function setFnName(string $fnName): void
    {
        $this->fnName = $fnName;
    }

    /**
     * 获取步骤记录
     * @return array
     */
    public function getSteps(): array
    {
        return $this->steps;
    }

    /**
     * 添加步骤记录
     * @param string $step 步骤
     * @param array $data 数据
     * @return $this
     */
    public function addSteps(string $step, array $data)
    {
        $this->steps[] = [
            'time' => datetime(),
            'step' => $step,
            'data' => $data,
        ];

        return $this;
    }

    /**
     * 获取日志数组
     * @return array
     */
    public function getLogger()
    {
        return [
            'title' => '执行步骤记录',
            'request_id' => $this->getRequestId(),
            'request_time' => request_time(),
            'fn_name' => $this->getFnName(),
            'steps' => $this->getSteps(),
        ];
    }
}