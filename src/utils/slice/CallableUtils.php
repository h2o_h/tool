<?php

namespace h\tool\utils\slice;

/**
 * 回调类
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/15
 */
class CallableUtils extends StaticUtils
{
    protected $callable;

    protected $result = null;

    protected ?\Throwable $e = null;

    protected bool $isThrow = true;

    /**
     * 设置回调
     * @param callable $callable
     * @return $this
     */
    public function setCallable(callable $callable)
    {
        $this->callable = $callable;
        return $this;
    }

    /**
     * 设置是否抛出异常
     * @param bool $isThrow
     * @return $this
     */
    public function setIsThrow(bool $isThrow)
    {
        $this->isThrow = $isThrow;
        return $this;
    }

    /**
     * 获取结果
     * @return mixed|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * 获取异常类
     * @return \Throwable|null
     */
    public function getThrowable()
    {
        return $this->e;
    }

    /**
     * 获取异常信息数组
     * @return array
     */
    public function getThrowableArray(): array
    {
        if (empty($this->e)) {
            return [];
        }

        $msg = [
            'code' => $this->e->getCode(),
            'info' => $this->e->getMessage(),
            'file' => $this->e->getFile(),
            'line' => $this->e->getLine(),
        ];

        return $msg;
    }

    /**
     * 获取异常信息
     * @return string
     */
    public function getThrowableInfo(): string
    {
        if (empty($this->e)) {
            return '';
        }

        $error = sprintf('%s in %s on line %d.', $this->e->getMessage(), $this->e->getFile(), $this->e->getLine());
        return $error;
    }

    /**
     * 执行
     * @return void
     */
    public function execute()
    {
        try {
            $this->result = call_user_func($this->callable);
        } catch (\Throwable $e) {
            $this->e = $e;
        }
        $this->throw();
    }

    private function throw()
    {
        if ($this->e) {
            throw $this->e;
        }
    }
}