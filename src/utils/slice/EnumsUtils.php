<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\IEnums;

/**
 * 枚举
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/27
 */
abstract class EnumsUtils implements IEnums
{
    /**
     * 获取枚举名称
     * @return string
     */
    public static function name(): string
    {
        return '枚举值';
    }

    /**
     * 定义枚举值和标签的关联数组
     * @return array
     */
    public static function describe(): array
    {
        return [];
    }

    /**
     * 获取枚举值和标签的关联数组
     * @return array
     */
    final public static function labelValue()
    {
        return label_value(static::describe());
    }

    /**
     * 获取枚举标签
     * @param string|int|float $value 枚举值
     * @return mixed|string
     */
    final public static function getLabelByValue($value)
    {
        $describe = self::describe();

        $label = $describe[$value] ?? '';

        return $label;
    }

    /**
     * 获取枚举值
     * @param string $label 枚举标签
     * @return int|string|null
     */
    final public static function getValueByLabel(string $label)
    {
        $describe = array_flip(self::describe());

        $value = $describe[$label] ?? null;

        return $value;
    }
}