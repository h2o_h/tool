<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\IDataProcess;

/**
 * 数据加工厂
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/27
 */
abstract class DataProcessUtils extends SingleUtils implements IDataProcess
{
    /**
     * @var array 原始数据
     */
    protected array $sourceData = [];

    /**
     * @var array 加工后的数据
     */
    protected array $processData = [];

    /**
     * @var array 转换器
     */
    private array $_converters = [];

    /**
     * 定义转换器
     * @return mixed
     */
    abstract protected function defConverters();

    /**
     * 需要删除的字段
     * @return array
     */
    protected function unsetFields(): array
    {
        return [];
    }

    /**
     * 需要转换的json字段
     * @return array
     */
    protected function jsonFields(): array
    {
        return [];
    }

    /**
     * 增加转换器
     * @param string $fromField 从字段
     * @param string|bool $toField 到字段
     * @param \Closure|null $closure 闭包处理
     * @return $this
     */
    final protected function addConverters(string $fromField, $toField, \Closure $closure = null)
    {
        if (empty($fromField) || empty($toField) || empty($closure)) {
            return $this;
        }

        $toField = $this->getToField($fromField, $toField);

        $this->_converters[] = [
            'fromField' => $fromField,
            'toField' => $toField,
            'closure' => $closure,
        ];

        return $this;
    }

    /**
     * 获取到字段
     * @param string $fromField 从字段
     * @param string|bool $toField 到字段
     * @return string
     */
    private function getToField(string $fromField, $toField): string
    {
        if (is_string($toField)) {
            return $toField;
        }

        if (is_bool($toField) && $toField) {
            return $fromField . '_text';
        }

        return $fromField;
    }

    /**
     * 获取加工后的数据
     * @return array
     */
    final public function getProcessData(): array
    {
        return $this->processData;
    }

    /**
     * 设置源数据
     * @param array $data
     * @return $this|mixed
     */
    final public function setSourceData(array $data)
    {
        $this->sourceData = $data;

        $this->processData = $data;

        return $this;
    }

    /**
     * 转换
     * @return $this|DataProcessUtils
     */
    final public function converter()
    {
        $this->defConverters();

        $this->runConverters();

        $this->handleProcessData();

        return $this;
    }

    /**
     * 执行转换
     * @return void
     */
    private function runConverters()
    {
        foreach ($this->_converters as $converter) {
            $fromField = $converter['fromField'];
            $toField = $converter['toField'];
            $closure = $converter['closure'];

            if (!isset($this->processData[$fromField])) {
                continue;
            }

            $value = $this->processData[$fromField];
            $this->processData[$toField] = call_user_func($closure, $value);
        }
    }

    private function handleProcessData()
    {
        foreach ($this->jsonFields() as $field) {
            $value = $this->processData[$field] ?? [] ?: [];
            if (empty($value)) {
                $value = [];
            } else if (is_string($value)) {
                $value = json_decode($value, true);
            } else if (is_object($value)) {
                $value = json_decode(json_encode($value), true);
            }

            $this->processData[$field] = $value;
        }

        foreach ($this->unsetFields() as $field) {
            unset($this->processData[$field]);
        }
    }
}