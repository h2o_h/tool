<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\to\IToDbArray;
use h\tool\utils\helper\NomenclatureHelper;

/**
 * 具有动态属性的通用空类-Db
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 * @inheritDoc 继承此类时，属性定义应统一使用“驼峰命名”或“下划线命名”中的一种,以保证转换后的key值唯一性
 */
class StdDbUtils extends \stdClass implements IToDbArray
{
    /**
     * 转换为db数组
     * - 属性名转换为下划线命名
     * - 存在不会覆盖替换
     *  - eg: $userName == $user_name, 定义$userName时, $user_name不会被覆盖
     * @return array
     */
    public function toDbArray(): array
    {
        $vars = get_object_vars($this);

        if (empty($vars)) return [];

        $res = [];
        ksort($vars);
        foreach ($vars as $key => $val) {
            $f = NomenclatureHelper::case2Underscore($key);
            if (!isset($res[$f])) {
                $res[$f] = $val;
            }
        }

        return $res;
    }
}