<?php

namespace h\tool\utils\slice;

use h\tool\interfaces\IInstance;

/**
 * 单例模式
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
abstract class SingleUtils implements IInstance
{
    private static $instance = [];

    /**
     * 获取实例
     * @return static
     */
    final public static function getInstance(): object
    {
        $class = get_called_class();
        $key = md5($class);

        if (empty(self::$instance[$key])) {
            self::$instance[$key] = new $class();
        }

        return self::$instance[$key];
    }

    /**
     * 初始化
     * @return void
     */
    protected function init()
    {

    }

    private function __construct()
    {
        $this->init();
    }

    private function __clone()
    {

    }

    private function __wakeup()
    {
    }
}