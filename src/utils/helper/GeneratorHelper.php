<?php

namespace h\tool\utils\helper;

use h\tool\utils\slice\PwdUtils;

/**
 * 生成器助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/22
 */
class GeneratorHelper
{
    /**
     * 生成单号
     * @inheritdoc 功能说明
     * 不包含前缀和包含微妙 => 15位(示例: 231215000010001)
     * 不包含前缀和微妙 => 11位(示例: 23121500001)
     *
     * @inheritdoc 生成规则: 前缀+(2|4位)年月日+秒+微妙
     * @param string $prefix 前缀
     * @param bool $has_ws 是否包含微妙, 默认true
     * @param string $format_date 日期格式
     * @return string
     */
    public static function sn(string $prefix = '', bool $has_ws = true, string $format_date = 'ymd')
    {
        $timestamp = microtime(true);

        list($hours, $minutes, $seconds) = explode(':', date('H:i:s', $timestamp));

        $date = date($format_date, $timestamp);

        $ds = ($hours * 3600) + ($minutes * 60) + $seconds;
        $ds = str_pad($ds, 5, '0', STR_PAD_LEFT);

        $ws = '';
        $has_ws && $ws = substr($timestamp * 10000, -4);

        return $prefix . $date . $ds . $ws;
    }

    /**
     * 生成变量名
     * @param string $prefix 前缀
     * @return string
     */
    public static function var(string $prefix = '')
    {
        $slice = PwdUtils::getInstance()->setLenSpecial(0)->setLenNumbers(0);

        $letters = $slice->setLenLcLetters(3)->setLenUcLetters(3)->gen();
        $nums = $slice->setLenNumbers(3)->setLenLcLetters(2)->setLenUcLetters(1)->gen();

        $string = substr($letters, 0, 3) . $nums . substr($letters, 3, 3);

        return $prefix . $string;
    }

    /**
     * 生成uuid
     * @param string $prefix 前缀
     * @param bool $toUpper 是否转大写
     * @return string
     */
    public static function uuid(string $prefix = '', bool $toUpper = false)
    {
        $uuid = sha1(md5(microtime(true)) . uniqid($prefix));

        $str = md5($prefix . $uuid);

        return $toUpper ? strtoupper($str) : $str;
    }
}