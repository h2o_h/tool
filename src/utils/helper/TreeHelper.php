<?php

namespace h\tool\utils\helper;

/**
 * 树助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/22
 */
class TreeHelper
{
    /**
     * 目录树
     * @param array $data 数据
     * @param int|string $pid pid
     * @param string $subName 子类名，默认:sub
     * @param string $fullName 全名
     * @param string $nameField 字段名
     * @return array
     */
    public static function gen(array $data, $pid = 0, string $subName = 'sub', string $fullName = '', string $nameField = 'name')
    {
        if (empty($data)) {
            return $data;
        }
        $tree = [];
        foreach ($data as $k => $v) {
            if ($pid == 0) {
                $fn = $v[$nameField];
            } else {
                $fn = $fullName . '/' . $v[$nameField];
            }
            $v['full_name'] = $fn;
            if ($v['pid'] == $pid) {
                $sub = self::gen($data, $v['id'], $subName, $fn, $nameField);
                if (array_key_exists($subName, $v)) {
                    $v[$subName] = array_merge($v[$subName], $sub);
                } else {
                    $v[$subName] = $sub;
                }
                $tree[] = $v;
            }
        }
        return $tree;
    }

    /**
     * 解析分类树
     * @param array $tree 分类树
     * @param string $subName 子名称字段
     * @param bool $isPkKey 是否主键key
     * @param string $pk 主键字段
     * @return array
     */
    public static function parse(array $tree, string $subName = 'sub', bool $isPkKey = true, string $pk = 'id')
    {
        if (empty($tree)) {
            return $tree;
        }

        $data = [];
        foreach ($tree as $item) {
            $sub = $item[$subName] ?? [];
            unset($item[$subName]);
            if (!empty($sub)) {
                $list = self::parse($sub, $subName);
                $data = $data + $list;
            }
            if ($isPkKey) {
                $data[$item[$pk]] = $item;
            } else {
                $data[] = $item;
            }
        }
        ksort($data);
        return $data;
    }
}