<?php

namespace h\tool\utils\helper;

/**
 * 数组助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/22
 */
class ArrayHelper
{
    /**
     * 过滤值为空的元素
     * @param array $arr 数组
     * @param bool $filterTrim 是否过滤空字符串
     *  - 默认：false
     * @return array
     */
    public static function filterValueIsNull(array $arr, bool $filterTrim = false)
    {
        $ret = [];
        $filterTrim && filter_array_trim($arr);
        foreach ($arr as $key => $value) {
            if (is_empty($value)) {
                continue;
            }
            $ret[$key] = $value;
        }

        return $ret;
    }

    /**
     * 数组值转成格式化类型值
     * @param array $arr 数组
     * @param bool $valueIsArrToJson 值是否数组转成json
     *  - 默认：true
     * @param bool $filterTrim 是否过滤空字符串
     *   - 默认：false
     * @return array
     */
    public static function convertValueForamtTypeValue(array $arr, bool $valueIsArrToJson = true, bool $filterTrim = false)
    {
        $filterTrim && filter_array_trim($arr);
        foreach ($arr as &$value) {
            if (is_numeric($value)) {
                if (strpos($value, '.') !== false) {
                    $value = floatval($value);
                } else {
                    $value = intval($value);
                }
            }

            if (is_callable($value) || is_object($value)) {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            }

            if (is_empty($value) && !is_array($value)) {
                $value = null;
            }

            if (is_array($value)) {
                $value = self::convertValueForamtTypeValue($value, $valueIsArrToJson);
                if ($valueIsArrToJson) {
                    $value = json_encode($value, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                }
            }
        }

        return $arr;
    }

    /**
     * 将数组中每一个值转换为指定类型值
     * @param array $arr 数组
     * @param \Closure|string $type 类型
     *  - Callback: 回调函数，接收数组作为参数，返回转换后的值
     *  - string: 转换后的类型，支持 'string', 'int', 'float', 'double', 'bool'
     * @return mixed
     * @throws \Exception
     */
    public static function mapTypeValue(array $arr, $type = 'string')
    {
        if ($type instanceof \Closure) {
            return call_user_func($type, $arr);
        }

        if (!is_string($type)) {
            throw new \Exception('type must be string');
        }

        $type = strtolower($type);
        $functions = [
            'string' => 'strval',
            'int' => 'intval',
            'float' => 'floatval',
            'double' => 'doubleval',
            'bool' => 'boolval',
        ];

        if (array_key_exists($type, $functions)) {
            $fn = $functions[$type];
        } else if (in_array($type, $functions)) {
            $fn = $type;
        } else {
            $fn = 'strval';
        }

        return array_map($fn, $arr);
    }

    /**
     * 过滤数组指定字段
     * @param array $arr 原数组
     * @param array $allowKeys 允许字段
     * @param array $unsetKeys 删除字段
     * @param \Closure|null $closure 闭包回调
     * - 闭包参数：不为空时，回调：$closure($arr, $allowKeys, $unsetKeys)
     * @return array|mixed
     */
    public static function filterKeys(array $arr, array $allowKeys = [], array $unsetKeys = [], \Closure $closure = null)
    {
        if (empty($allowKeys)) return $arr;
        if (empty($unsetKeys)) return $arr;
        if ($closure && $closure instanceof \Closure) return call_user_func($closure, $arr, $allowKeys, $unsetKeys);

        $unsetKeys = array_flip($unsetKeys);
        $allowKeys = array_flip($allowKeys);
        $callback = function (array $item) use ($allowKeys, $unsetKeys) {
            $item = array_diff_key($item, $unsetKeys);
            $item = array_intersect_key($item, $allowKeys);
            return $item;
        };

        $retArr = [];
        $firstKey = array_key_first($arr);
        if (is_int($firstKey)) {
            foreach ($arr as $item) {
                $retArr[] = $callback($item);
            }
        } else if (is_string($firstKey)) {
            $retArr = $callback($arr);
        } else {
            return $arr;
        }

        return $retArr;
    }

    /**
     * 转字符串
     * @param array $arr 数组
     * @param string $separator 分隔符
     *  - 默认分隔符为";"
     * @return string
     */
    public static function toString(array $arr, string $separator = ';')
    {
        $str = [];
        foreach ($arr as $key => $value) {
            if (is_string($key)) {
                $str[] = $key . ':' . $value;
            } else {
                $str[] = $value;
            }
        }

        return join($separator, $str);
    }

    /**
     * 将变量转换为数组
     * @param $var
     * - 分割符: ',', '，', '|', "\n", "\t", "\r", "\r\n"
     * @return array|int[]|string[]
     */
    public static function toArray($var)
    {
        if (is_array($var)) return $var;
        if (is_int($var)) return [$var];

        if (is_object($var)) return json_decode(json_encode($var), true);

        if (is_string($var)) {
            if (trim($var) === '') {
                return [];
            } else {
                $var = str_replace([',', '，', '|', "\n", "\t", "\r", "\r\n"], ',', $var);
                return explode(',', $var);
            }
        }

        if (empty($var)) return [];
    }

    /**
     * 生成A-Z组合数组
     * @return array
     */
    public static function rangeAz()
    {
        $s = 'A';
        $count = 26 * 26;
        $array = [];
        while ($count--) {
            $array[] = $s;
            $s++;
        }
        return $array;
    }

    /**
     * 数组去重
     * @param array $arr
     * @return array
     */
    public static function uniqueValue(array $arr)
    {
        return array_values(array_filter(array_unique($arr)));
    }

    /**
     * 比较两数组是否相等
     * @param array $arr1
     * @param array $arr2
     * @return bool
     */
    public static function compare(array $arr1, array $arr2)
    {
        sort($arr1);
        sort($arr2);
        if (($arr1 <=> $arr2) == 0 || $arr1 == $arr2) {
            return true;
        }

        return false;
    }

    /**
     * 模板变量替换
     * @param string $content 内容
     * @param array $vars 变量
     * @param string $sprintf 格式化
     *  - {%s} 表示变量
     * @return string
     */
    public static function tplVarsReplace(string $content, array $vars, string $sprintf = '{%s}')
    {
        if (empty($vars)) {
            return $content;
        }

        $vars = self::varsSprintf($vars, $sprintf);

        return strtr($content, $vars);
    }

    /**
     * 变量格式化
     * @param array $vars 格式化
     * @param string $sprintf 格式化
     *  - {%s} 表示变量
     * @return array
     */
    public static function varsSprintf(array $vars, string $sprintf = '{%s}')
    {
        $replace = [];
        array_walk($vars, function ($val, $key) use (&$replace, $sprintf) {
            $newKey = sprintf($sprintf, $key);
            $replace[$newKey] = $val;
        });
        $vars = $replace;

        return $vars;
    }
}