<?php

namespace h\tool\utils\helper;

/**
 * 字符串助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/7
 */
class StrHelper
{
    /**
     * 打码
     * @param string $str 字符串
     * @param int $start 起始位
     * @param int $end 结束位
     * @param string $maskChar 打码字符
     *  - 默认：*
     * @param int $repeatCount 重复次数
     *  - 默认：0, 特殊值,自动计算起始至结束的字符长度
     * @param string $encoding 字符编码
     *  - 默认：UTF-8
     * @return string
     */
    public static function mask(string $str, int $start, int $end, string $maskChar = '*', int $repeatCount = 0, string $encoding = 'UTF-8'): string
    {
        $strLen = mb_strlen($str, $encoding);
        if ($strLen < $start) {
            return $str;
        }

        if ($strLen < $end) {
            $end = $strLen;
        }

        if (empty($repeatCount)) {
            $repeatCount = abs($end - $start) + 1;
        }

        $maskStr = mb_substr($str, 0, $start - 1, $encoding);
        $maskStr .= str_repeat($maskChar, $repeatCount);
        $maskStr .= mb_substr($str, $end, null, $encoding);

        return $maskStr;
    }

    /**
     * 字符串长度超过指定长度时，用指定字符串替换
     * @param string $str 字符串
     * @param int $length 指定长度
     *  - 默认：0, 不进行处理
     * @param string $replaceStr 替换字符串
     *  - 默认：...
     * @param string $encoding 字符编码
     *  - 默认：UTF-8
     * @return string
     */
    public static function replaceGtLength(string $str, int $length = 0, string $replaceStr = '...', string $encoding = 'UTF-8')
    {
        if ($length == 0) {
            return $str;
        }

        if (mb_strlen($str) > $length) {
            $str = mb_substr($str, 0, $length, $encoding) . $replaceStr;
        }

        return $str;
    }
}