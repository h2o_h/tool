<?php

namespace h\tool\utils\helper;

/**
 * 时间助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/22
 */
class DateHelper
{
    /**
     * 返回两个时间相差天数
     * @param string $startTime
     * @param string $entTime
     * @return int
     */
    public static function dateDiffDay(string $startDateTime, string $entDateTime)
    {
        $start = date_create($startDateTime);

        $end = date_create($entDateTime);

        $diff = date_diff($start, $end);

        return (int)$diff->format("%a");
    }

    /**
     * 根据指定日期获取周一周日的日期
     * @param string $date 指定日期
     * @param string $format 格式化
     * @return array
     */
    public static function getWeekStartEndDateByDate(string $date, string $format = 'Y-m-d')
    {
        $time = strtotime($date);
        // 星期几
        $dayOfWeek = date('N', $time);

        // 计算需要减去的天数以获得周一日期
        $daysToMonday = $dayOfWeek - 1;

        // 计算需要添加的天数以获得周日日期
        $daysToSunday = 7 - $dayOfWeek;

        // 周一
        $mondayDate = date($format, strtotime("-$daysToMonday days", $time));

        // 周日
        $sundayDate = date($format, strtotime("+$daysToSunday days", $time));

        return [$mondayDate, $sundayDate];
    }

    /**
     * 以当前时间获取指定周起始日期
     * @param int $i
     * @param string $format
     * @return array
     */
    public static function getWeekStartEndDateByNow(int $i = 0, string $format = 'Y-m-d')
    {
        if ($i > 0) {
            $i += 1;
        }

        $nowWeek = self::getWeekStartEndDateByDate(date($format, time()));
        $mondayDate = strtotime(current($nowWeek));

        // 一周的开始和结束日期
        $weekStart = strtotime("{$i} Monday", $mondayDate);
        $weekEnd = strtotime("{$i} Sunday", $mondayDate);

        $weekStart = date($format, $weekStart);
        $weekEnd = date($format, $weekEnd);

        return [$weekStart, $weekEnd];
    }
}