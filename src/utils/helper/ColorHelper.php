<?php

namespace h\tool\utils\helper;

/**
 * 颜色助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
class ColorHelper
{
    /**
     * 随机生成rgb颜色
     * @param string $suffix 后缀
     * @return string
     */
    public static function rgb(string $suffix = '')
    {
        $r = mt_rand(0, 255);
        $g = mt_rand(0, 255);
        $b = mt_rand(0, 255);

        return sprintf('rgb(%s, %s, %s)', $r, $g, $b) . $suffix;
    }

    /**
     * 随机生成rgba颜色
     * @param string $suffix 后缀
     * @return string
     */
    public static function rgba(string $suffix = '')
    {
        $r = mt_rand(0, 255);
        $g = mt_rand(0, 255);
        $b = mt_rand(0, 255);
        $a = mt_rand(0, 100) / 100;

        return sprintf('rgba(%s, %s, %s, %s)', $r, $g, $b, $a) . $suffix;
    }

    /**
     * 随机生成16进制颜色
     * @param string $suffix 后缀
     * @return string
     */
    public static function hex(string $suffix = '')
    {
        $r = mt_rand(0, 255);
        $g = mt_rand(0, 255);
        $b = mt_rand(0, 255);

        return strtoupper(sprintf('#%s%s%s', dechex($r), dechex($g), dechex($b))) . $suffix;
    }

    /**
     * 随机生成cmyk颜色
     * @return string
     */
    public static function cmyk()
    {
        $c = mt_rand(0, 100);
        $m = mt_rand(0, 100);
        $y = mt_rand(0, 100);
        $k = mt_rand(0, 100);

        return sprintf('cmyk(%s, %s, %s, %s)', $c, $m, $y, $k);
    }
}