<?php

namespace h\tool\utils\helper;

/**
 * 金额助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/9
 */
class AmountHelper
{
    /**
     * 显示金额带币种
     * @param int|float|double $amount 金额
     * @param string $currency 币种
     *  - 默认: CNY
     *  - 币种列表：https://www.iban.com/currency-codes
     * @param bool $isRear 是否币种在后
     *  - 默认：true
     * @param string $sprintfFormat 显示格式
     *  - 使用函数sprintf处理的字符串格式，例如：'%s %s'
     * @return string
     * @example showCurrency(100, 'CNY', false, '%s %s')
     * @example showCurrency(100, 'CNY', true, '%s %s')
     */
    public static function showCurrency($amount, string $currency = 'CNY', bool $isRear = true, string $sprintfFormat = '%s %s')
    {
        if (!$isRear) {
            return sprintf($sprintfFormat, $currency, $amount);
        }
        return sprintf($sprintfFormat, $amount, $currency);
    }

    /**
     * 常见币种数据
     * @return string[]
     * @link http://www.uaec-expo.com/hb.html
     */
    public static function currency(): array
    {
        return [
            [
                'code' => 'CNY',
                'name' => '人民币',
                'symbol' => '¥',
                'decimal' => 2,
            ],
            [
                'code' => 'HKD',
                'name' => '香港元',
                'symbol' => '$',
                'decimal' => 2,
            ],
            [
                'code' => 'USD',
                'name' => '美元',
                'symbol' => '$',
                'decimal' => 2,
            ],
            [
                'code' => 'EUR',
                'name' => '欧元',
                'symbol' => '€',
                'decimal' => 2,
            ],
            [
                'code' => 'GBP',
                'name' => '英镑',
                'symbol' => '£',
                'decimal' => 2,
            ],
            [
                'code' => 'CHF',
                'name' => '瑞士法郎',
                'symbol' => 'Fr',
                'decimal' => 2,
            ],
            [
                'code' => 'JPY',
                'name' => '日元',
                'symbol' => '¥',
                'decimal' => 2,
            ],
            [
                'code' => 'AUD',
                'name' => '澳大利亚元',
                'symbol' => '$',
                'decimal' => 2,
            ],
            [
                'code' => 'SGD',
                'name' => '新加坡元',
                'symbol' => '$',
                'decimal' => 2,
            ],
            [
                'code' => 'CAD',
                'name' => '加拿大元',
                'symbol' => '$',
                'decimal' => 2,
            ],
        ];
    }
}