<?php

namespace h\tool\utils\helper;

/**
 * 安全助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/22
 */
class SafeHelper
{
    /**
     * 加密
     * @param string $str 待加密字符串
     * @param string $key 盐值
     * @return string
     */
    public static function encry(string $str, string $key = '^2!0%2&4@$')
    {
        return md5(sha1($key . $str . md5($str . $key)) . substr(md5($str . $key), 8, 16));
    }
}