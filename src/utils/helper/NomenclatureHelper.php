<?php

namespace h\tool\utils\helper;

/**
 * 命名法助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/2
 */
class NomenclatureHelper
{
    /**
     * 将多个连续空格替换为下划线
     * @param string $str
     * @return string
     */
    public static function convertSpaces2Underscore(string $str): string
    {
        $str = preg_replace('/\s+/', '_', $str);
        return $str;
    }

    /**
     * 下划线转换为小驼峰命名法
     * @param string $str
     * @return string
     */
    public static function underscore2CamelCase(string $str): string
    {
        $str = self::convertSpaces2Underscore($str);
        $str = str_replace('_', '', ucwords($str, '_ '));
        return lcfirst($str);
    }

    /**
     * 下划线转换为大驼峰命名法
     * @return string
     */
    public static function underscore2PascalCase(string $str): string
    {
        $str = self::convertSpaces2Underscore($str);
        $str = str_replace('_', '', ucwords($str, '_'));
        return $str;
    }

    /**
     * 驼峰命名法转换为下划线
     * @param string $str
     * @return string
     */
    public static function case2Underscore(string $str): string
    {
        $str = self::convertSpaces2Underscore($str);
        $str = preg_replace('/([a-z])([A-Z])/', '$1_$2', $str);
        return strtolower($str);
    }
}