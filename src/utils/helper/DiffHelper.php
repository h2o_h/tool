<?php

namespace h\tool\utils\helper;

/**
 * 差值助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/11
 */
class DiffHelper
{
    /**
     * 常规
     * @param int|float $var1
     * @param int|float $var2
     * @param bool $isForward 是否是正向,默认是true
     *  - 正向: $var1 - $var2
     *  - 反向: $var2 - $var1
     * @return float
     */
    public static function general($var1, $var2, bool $isForward = true)
    {
        empty($var1) && $var1 = 0;
        empty($var2) && $var2 = 0;

        $value = $isForward ? ($var1 - $var2) : ($var2 - $var1);
        return floatval($value);
    }

    /**
     * 绝对值
     * @param int|float $var1
     * @param int|float $var2
     * @param bool $isForward 是否是正向,默认是true
     *  - 正向: $var1 - $var2
     *  - 反向: $var2 - $var1
     * @return float|int
     */
    public static function abs($var1, $var2, bool $isForward = true)
    {
        return abs(self::general($var1, $var2, $isForward));
    }
}