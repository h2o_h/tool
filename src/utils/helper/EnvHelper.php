<?php

namespace h\tool\utils\helper;

/**
 * 环境助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/3
 */
class EnvHelper
{
    /**
     * 获取当前环境
     * @param string $env
     * @return bool
     */
    public static function setEnv(string $env): bool
    {
        set_env($env);
        return true;
    }

    /**
     * 判断是否为本地环境
     * @return bool
     */
    public static function isLocal(): bool
    {
        return constant('C_ENV_LOCAL') === constant('C_ENV');
    }

    /**
     * 判断是否为开发环境
     * @return bool
     */
    public static function isDev(): bool
    {
        return constant('C_ENV_DEV') === constant('C_ENV');
    }

    /**
     * 判断是否为测试环境
     * @return bool
     */
    public static function isTest(): bool
    {
        return constant('C_ENV_TEST') === constant('C_ENV');
    }

    /**
     * 判断是否为预发环境
     * @return bool
     */
    public static function isPre(): bool
    {
        return constant('C_ENV_PRE') === constant('C_ENV');
    }

    /**
     * 判断是否为生产环境
     * @return bool
     */
    public static function isProd(): bool
    {
        return constant('C_ENV_PROD') === constant('C_ENV');
    }

    /**
     * 判断是否为沙盒环境
     * @return bool
     */
    public static function isSandbox(): bool
    {
        return constant('C_ENV_SANDBOX') === constant('C_ENV');
    }
}