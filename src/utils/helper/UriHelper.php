<?php

namespace h\tool\utils\helper;

/**
 * URI助手
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/3
 */
class UriHelper
{
    /**
     * 生成路由地址
     * @param string $host 主机地址
     * @param string $path 路由地址
     * @param array $query 查询参数
     * @param string $anchorages 锚点
     * @return string
     */
    public static function genRoute(string $host, string $path, array $query = [], string $anchorages = ''): string
    {
        $host = rtrim(trim($host), DIRECTORY_SEPARATOR);
        if (empty($host)) {
            throw new \Exception('host不能为空');
        }

        filter_array_trim($query);

        $uri = $host . DIRECTORY_SEPARATOR . ltrim(trim($path), DIRECTORY_SEPARATOR);
        $queryStr = $query ? C_QUESTION_MARK . http_build_query($query) : '';
        $anchorages = trim(trim($anchorages), C_QUESTION_MARK);

        $route = $uri . $queryStr . ($anchorages ? C_QUESTION_MARK . $anchorages : '');

        return $route;
    }
}