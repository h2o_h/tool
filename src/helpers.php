<?php

if (!function_exists('h_version')) {
    /**
     * 版本
     * @return string
     */
    function h_version()
    {
        return constant('H_TOOL_VERSION');
    }
}

if (!function_exists('set_env')) {
    /**
     * 设置环境
     * @param string $env
     * @return void
     */
    function set_env(string $env = C_ENV_LOCAL)
    {
        defined('C_ENV') or define('C_ENV', strtolower($env));
    }
}

if (!function_exists('request_time')) {
    /**
     * 请求时间
     * @return float
     */
    function request_time()
    {
        if (!isset($_SERVER['REQUEST_TIME'])) {
            $_SERVER['REQUEST_TIME'] = microtime(true);
        }
        return $_SERVER['REQUEST_TIME'];
    }
}

if (!function_exists('datetime')) {
    /**
     * 获取日期时间
     * @param int $time 时间戳
     * @param string $format 格式化,默认:(Y-m-d H:i:s)
     * @return false|string
     */
    function datetime(int $time = 0, string $format = C_TIME_FORMAT_YMD_HIS)
    {
        return date($format, $time ?: time());
    }
}

if (!function_exists('is_empty')) {
    /**
     * 判断是否为空
     * @param string|array|null $value
     * @return bool
     */
    function is_empty($value)
    {
        return $value === C_EMPTY_STRING ||
            $value === C_EMPTY_ARRAY ||
            $value === C_EMPTY_NULL ||
            $value === C_EMPTY_OBJECT ||
            (is_string($value) && strcasecmp($value, C_EMPTY_UNDEFINED) === 0) ||
            (is_string($value) && strcasecmp($value, C_EMPTY_STR_NULL) === 0);
    }
}

if (!function_exists('is_cli')) {
    /**
     * 是否是命令行模式
     * @return bool
     */
    function is_cli()
    {
        return php_sapi_name() === 'cli';
    }
}

if (!function_exists('ini_set_limit')) {
    /**
     * 设置php.ini的内存和执行时间限制
     * @return void
     */
    function ini_set_limit()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }
}

if (!function_exists('filter_array_trim')) {
    /**
     * 过滤数组中两边的空格
     * @param array $input 数据
     * @return array
     */
    function filter_array_trim(array &$input = [])
    {
        foreach ($input as &$value) {
            if (is_array($value)) {
                $value = filter_array_trim($value);
            }
            if (is_string($value)) {
                $value = trim($value);
            }
        }
        return $input;
    }
}

if (!function_exists('is_datetime_in_range')) {
    /**
     * 判断一个时间是否在两个时间之间
     * @param string $dateTime 判断时间
     * @param string $startDateTime 开始时间
     * @param string $endDateTime 结束时间
     * @return bool
     */
    function is_datetime_in_range(string $dateTime, string $startDateTime, string $endDateTime)
    {
        $timestamp = strtotime($dateTime);
        $startTimestamp = strtotime($startDateTime);
        $endTimestamp = strtotime($endDateTime);

        return $timestamp >= $startTimestamp && $timestamp <= $endTimestamp;
    }
}

if (!function_exists('label_value')) {
    /**
     * 格式化label_value数据
     * @param array $dataArray
     * @param string $labelName
     * @param string $valueName
     * @return array
     */
    function label_value(array $dataArray, string $labelName = 'label', string $valueName = 'value')
    {
        $formattedArray = array();
        foreach ($dataArray as $key => $value) {
            if (is_array($value)) {
                $formattedValue = label_value($value);
                $formattedArray[$key] = $formattedValue;
            } else {
                $formattedValue = array(
                    $labelName => $value,
                    $valueName => $key
                );

                $formattedArray[] = $formattedValue;
            }
        }
        return $formattedArray;
    }
}

if (!function_exists('echo_json')) {
    /**
     * 输出json数据
     * @param array|object|string|float|mixed $data 数据
     * @param bool $headerJson 是否添加json header
     *  - 默认：true
     * @param bool $die 是否结束
     *  - 默认：true
     * @return void
     */
    function echo_json($data, bool $headerJson = true, bool $die = true)
    {
        ($headerJson && !is_cli()) && header('Content-Type: application/json');

        echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $die && die();
    }
}

if (!function_exists('util_callable')) {
    /**
     * 获取CallableUtils实例
     * @return \h\tool\utils\slice\CallableUtils
     */
    function util_callable()
    {
        $obj = \h\tool\utils\slice\CallableUtils::getInstance();
        return $obj;
    }
}