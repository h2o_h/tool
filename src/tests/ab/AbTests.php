<?php

namespace h\tool\tests\ab;

use h\tool\utils\slice\StaticUtils;

/**
 * AbTests
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */
abstract class AbTests extends StaticUtils
{
    abstract public function run();

    final public function stdout(array $data, string $title = '')
    {
        $json = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $str = sprintf('%s: %s', $title, $json) . PHP_EOL;
        echo $str;
    }
}