<?php

namespace h\tool\tests;

use h\tool\tests\ab\AbTests;
use h\tool\utils\slice\PrizeUtils;

/**
 * PrizeTest
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */
class PrizeTest extends AbTests
{
    public function run()
    {
        $obj = PrizeUtils::getInstance();
        $obj->addPrize($this->getNo(), 'iPhone 13-1部', 0.01, 1);
        $obj->addPrize($this->getNo(), '华为平板-1台', 2.00, 5);
        $obj->addPrize($this->getNo(), '联想笔记本-1台', 3.00, 10);
        $obj->addPrize($this->getNo(), '华为手表-1个', 5.00, 50);
        $obj->addPrize($this->getNo(), '无线蓝牙耳机-1副', 9.99, 145);
        $obj->addPrize($this->getNo(), '有线耳机-1条', 80.00, 199);
        $obj->drawPrize();
        $prize = $obj->getWinPrize();

        return $prize;
    }

    private function getNo()
    {
        return md5(uniqid());
    }
}


for ($i = 1; $i <= mt_rand(1, 100); $i++) {
    $test = new PrizeTest();
    $prize = $test->run();
    $test->stdout($prize, sprintf('第 %d 次抽奖', $i));
}