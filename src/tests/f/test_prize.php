<?php
/**
 * ${NAME}
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */


date_default_timezone_set('Asia/Shanghai');

/**
 * 静态模式
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/26
 */
abstract class StaticUtils
{
    /**
     * 获取实例
     * @return static
     */
    final public static function getInstance()
    {
        $class = get_called_class();
        return new $class;
    }
}

/**
 * AbTests
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */
abstract class AbTests extends StaticUtils
{
    abstract public function run();

    final public function stdout(array $data, string $title = '')
    {
        $json = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $str = sprintf('%s: %s', $title, $json) . PHP_EOL;
        echo $str;
    }
}

/**
 * 抽奖
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */
class PrizeUtils extends StaticUtils
{
    const F_PRIZE_NO = 'prize_no';
    const F_PRIZE_NAME = 'prize_name';
    const F_PRIZE_QUANTITIES = 'prize_quantities';
    const F_PRIZE_PROBABILITY = 'prize_probability';
    const F_PRIZE_PIC_PATH = 'prize_pic_path';

    private int $sumProbability = 100;

    /**
     * @var array 奖品列表
     */
    private array $_prizes = [

    ];

    /**
     * @var array 中奖信息
     */
    private array $_winPrize = [];

    /**
     * @var int|float 中奖数字
     */
    private $_winNumber = 0;

    /**
     * 增加奖品
     * @param string $no 编号
     * @param string $name 名称
     * @param int|float $probability 概率
     * @param int|float $quantities 数量
     * @param string $picPath 图片路径
     * @return $this
     */
    final public function addPrize(string $no, string $name, $probability = 0, $quantities = 1, string $picPath = '')
    {
        if (isset($this->_prizes[$no])) {
            return $this;
        }

        $this->_prizes[$no] = [
            self::F_PRIZE_NO => $no,
            self::F_PRIZE_NAME => $name,
            self::F_PRIZE_QUANTITIES => $quantities,
            self::F_PRIZE_PROBABILITY => $probability,
            self::F_PRIZE_PIC_PATH => $picPath,
        ];
        return $this;
    }

    /**
     * 移除奖品
     * @param $no
     * @return $this
     */
    final public function removePrize($no)
    {
        unset($this->_prizes[$no]);
        return $this;
    }

    /**
     * 获取中奖奖品
     * @return array
     */
    final public function getWinPrize()
    {
        return $this->_winPrize;
    }

    /**
     * 设置中奖信息
     * @param array $prize
     * @return $this
     */
    protected function setWinPrize(array $prize = [])
    {
        $this->_winPrize = $prize;
        return $this;
    }

    /**
     * 抽奖
     * @return $this
     */
    final public function drawPrize()
    {
        if (empty($this->_prizes)) {
            throw new \Exception('未设置奖品信息');
        }

        $this->sortPrizes();

        $randomNumber = $this->randNumberWithProbability();

        $this->winPrizeLogic($randomNumber);

        return $this;
    }

    /**
     * 中奖逻辑
     * @param int|float $randomNumber
     * @return void
     */
    private function winPrizeLogic($randomNumber)
    {
        // 根据随机数确定中奖奖品
        $probabilitySum = 0;
        foreach ($this->_prizes as $no => &$prize) {
            $probability = $prize[self::F_PRIZE_PROBABILITY];
            $quantities = $prize[self::F_PRIZE_QUANTITIES];
            if ($quantities <= 0) {
                break;
            }

            $probabilitySum += $probability;
            if ($randomNumber <= $probabilitySum) {
                $prize[self::F_PRIZE_QUANTITIES] -= 1;
                $prize['win_number'] = $this->_winNumber;
                $this->setWinPrize($prize);
                break;
            }
        }
    }

    /**
     * 根据概率生成随机数
     * @return int|float
     */
    private function randNumberWithProbability()
    {
        // 计算总概率
        $totalProbability = array_sum(array_column($this->_prizes, self::F_PRIZE_PROBABILITY));

        $totalProbability = number_format($totalProbability, 2);

        if (($totalProbability <=> $this->sumProbability) != 0) {
            throw new \Exception(sprintf('奖品概率总和(%s)不等于%s', $totalProbability, $this->sumProbability));
        }

        // 生成随机数
        $exp = 9;
        $randomNumber = round(mt_rand(1, pow(100, $exp)) / pow(100, $exp - 1), 4);

        $this->_winNumber = $randomNumber;

        return $randomNumber;
    }

    /**
     * 排序奖品
     * @return void
     */
    private function sortPrizes()
    {
        $sortFields = array_column($this->_prizes, self::F_PRIZE_PROBABILITY);
        array_multisort($sortFields, SORT_DESC, $this->_prizes);
    }
}

/**
 * PrizeTest
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/28
 */
class PrizeTest extends AbTests
{
    public function run()
    {
        $obj = PrizeUtils::getInstance();
//        $obj->addPrize($this->getNo(), 'iPhone 13-1部', 0.01, 1);
//        $obj->addPrize($this->getNo(), '华为平板-1台', 0.20, 2);
//        $obj->addPrize($this->getNo(), '联想笔记本-1台', 0.40, 3);
//        $obj->addPrize($this->getNo(), '华为手表-1个', 1.00, 4);
//        $obj->addPrize($this->getNo(), '无线蓝牙耳机-1副', 8.39, 10);
//        $obj->addPrize($this->getNo(), '有线耳机-1条', 10.00, 20);
//        $obj->addPrize($this->getNo(), '谢谢参与', 80.00, PHP_INT_MAX);

        $obj->addPrize($this->getNo(), 'SP宁荣荣', 0.01, 1);
        $obj->addPrize($this->getNo(), '3999钻', 0.02, 2);
        $obj->addPrize($this->getNo(), 'SSR伙伴', 0.40, 3);
        $obj->addPrize($this->getNo(), '神赐魂环', 0.60, 4);
        $obj->addPrize($this->getNo(), 'SP魂骨', 8.00, 10);
        $obj->addPrize($this->getNo(), 'SSR魂石', 10.97, 20);
        $obj->addPrize($this->getNo(), 'SR伙伴', 80.00, PHP_INT_MAX);



        $count = mt_rand(1, 100);
        $count = 10;

        $param = [
            'total_counts' => $count,
        ];

        $prizes = [];

        $this->stdout($param, '抽奖开始');
        for ($i = 1; $i <= $count; $i++) {
            $obj->drawPrize();
            $prize = $obj->getWinPrize();
            $prizes[] = $prize;
            $this->stdout($prize, sprintf('第 %d 次抽奖', $i));
        }

        $tj = array_column($prizes, 'prize_name');

        $tjCount = array_count_values($tj);

        asort($tjCount);
        $param['tj'] = $tjCount;

        $this->stdout($param, '抽奖结束');


        return $prize;
    }

    private function getNo()
    {
        return md5(uniqid());
    }
}


$test = new PrizeTest();
$prize = $test->run();

