<?php

namespace h\tool\interfaces;

/**
 * 实例接口
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/15
 */
interface IInstance
{
    public static function getInstance(): object;
}