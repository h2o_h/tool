<?php

namespace h\tool\interfaces;

/**
 * 属性设置与获取接口
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
interface ISetterGetter
{
    const PREFIX_SETTER = 'set';
    const PREFIX_GETTER = 'get';

    public function setter(string $attr, $value): self;

    public function getter(string $attr, $default = null);
}