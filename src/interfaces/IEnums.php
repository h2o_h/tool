<?php

namespace h\tool\interfaces;

/**
 * 枚举接口
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/27
 */
interface IEnums
{
    /**
     * 获取枚举名称
     * @return string
     */
    public static function name(): string;

    /**
     * 定义枚举值和标签的关联数组
     * @return array
     */
    public static function describe(): array;
}