<?php

namespace h\tool\interfaces\to;

/**
 * IToJson
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
interface IToJson
{
    /**
     * 转换为JSON字符串
     * @return string
     */
    public function toJson(): string;
}