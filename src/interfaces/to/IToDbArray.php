<?php

namespace h\tool\interfaces\to;

/**
 * IToDbArray
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
interface IToDbArray
{
    public function toDbArray(): array;
}