<?php

namespace h\tool\interfaces\to;

/**
 * IToArray
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
interface IToArray
{
    /**
     * 转换为数组
     * @return array
     */
    public function toArray(): array;
}