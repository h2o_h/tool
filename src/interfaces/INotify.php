<?php

namespace h\tool\interfaces;

/**
 * 通知接口
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/4/16
 */
interface INotify
{
    /**
     * 服务名称
     * @return string
     */
    public function serviceName(): string;

    /**
     * 消息标题
     * @return string
     */
    public function title(): string;

    /**
     * 消息内容
     * @return string
     */
    public function body(): string;

    /**
     * 事发时间
     * @return string
     */
    public function eventTime(): string;

    /**
     * 发送消息
     * @return bool
     */
    public function send(): bool;
}