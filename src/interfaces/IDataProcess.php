<?php

namespace h\tool\interfaces;

/**
 * 数据转换接口
 * @Author Smiler <smilerliu@sz2k.com>
 * @Date 2024/3/27
 */
interface IDataProcess
{
    /**
     * 设置源数据
     * @param array $data
     * @return mixed
     */
    public function setSourceData(array $data);

    /**
     * 转换器
     * @return $this
     */
    public function converter();

    /**
     * 获取加工后的数据
     * @return array
     */
    public function getProcessData(): array;
}